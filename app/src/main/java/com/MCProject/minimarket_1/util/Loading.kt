package com.MCProject.minimarket_1.util

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.MCProject.minimarket_1.R

/**
 * Classe volta a mostrare ed eliminare il form di caricamento
 */
class Loading(val activity: Activity) {

    private lateinit var dialog: AlertDialog
    var init = 0

    fun startLoading() {
        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater

        builder.setView(inflater.inflate(R.layout.loading_dialog, null))
        builder.setCancelable(true)//cliccando fuori dal loading lo annulla

        dialog = builder.create()
        init = 1
        dialog.show()
    }

    fun stopLoadingDialog(){
        if(init == 1)
            dialog.dismiss()
    }
}