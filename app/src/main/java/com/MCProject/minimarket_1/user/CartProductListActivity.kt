package com.MCProject.minimarket_1.user

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.MCProject.minimarket_1.MainActivity
import com.MCProject.minimarket_1.R
import com.MCProject.minimarket_1.gestor.MarketProductListActivity

/**
 * CLasse volta al far visualizzare i prodotti nel carrello di un determinato utente
 */
class CartProductListActivity: MarketProductListActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        type = "cart"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()

        titleTv.text = "Your Cart"
        addBtn.visibility = View.GONE
        checkoutBtn.visibility = View.VISIBLE
        checkoutBtn.text = ""
        checkoutBtn.background = resources.getDrawable(R.drawable.my_exit)
        checkoutBtn.setOnClickListener {
            //Perform the checkout
            if(productList.size > 0) {
                doOrderCheckout()
            } else {
                Toast.makeText(this, "There's no product in your cart", Toast.LENGTH_SHORT).show()
            }
        }
    }

    /**
     * Perform the order checkout
     */
    @RequiresApi(Build.VERSION_CODES.O)
    @Synchronized
    private fun doOrderCheckout() {
        MainActivity.frM.getGestor(this, "/profili/gestori/dati", productList[0].owner)
            .addOnCompleteListener { doc ->
                frO.uploadOrder(
                    productList,
                    this,
                    MainActivity.user!!.email,
                    null,
                    doc.result["via"].toString() +", "+ doc.result["citta"].toString()
                )
            }
    }
}
